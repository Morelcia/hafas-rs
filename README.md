# hafas-rs

Implementation of a HAFAS client in Rust. This is forked from [hafas-rs](https://cyberchaos.dev/yuka/hafas-rs/).

This takes a lot of inspiration from the JavaScript [hafas-client](https://github.com/public-transport/hafas-client) library, which is licensed under ISC.

## Profiles

The following profiles are currently supported:

| Short Name      | Long Name                                           |
|-----------------|-----------------------------------------------------|
| AVV             | Aachener Verkehrsverbund                            |
| BLS             | BLS AG                                              |
| CFL             | Société Nationale des Chemins de Fer Luxembourgeois |
| CMTA            | Austin, Texas                                       |
| DART            | Des Moines Area Rapid Transit                       |
| DB              | Deutsche Bahn                                       |
| HVV             | Hamburg public transport                            |
| INSA            | Nahverkehr Sachsen-Anhalt                           |
| Irish-Railway   | Iarnród Éireann                                     |
| Mobiliteit-Lu   | Mobilitéitszentral                                  |
| mobil-nrw       | mobil.nrw                                           |
| NVV             | Nordhessischer Verkehrsverbund                      |
| NahSH           | Nah.SH                                              |
| OÖVV            | Oberösterreichischer Verkehrsverbund                |
| ÖBB             | Österreichische Bundesbahnen                        |
| PKP             | Polskie Koleje Państwowe                            |
| RMV             | Rhein-Main-Verkehrsverbund                          |
| RSAG            | Rostocker Straßenbahn AG                            |
| Rejseplanen     | Rejseplanen in Denmark                              |
| S-Bahn-München  | S-Bahn München                                      |
| STV             | Steirischer Verkehrsverbund                         |
| SVV             | Salzburger Verkehrsverbund                          |
| saarVV          | Saarfahrplan/VGS Saarland                           |
| Salzburg        | Salzburg                                            |
| TPG             | Transports publics genevois                         |
| VBB             | Berlin &amp; Brandenburg public transport           |
| VBN             | Verkehrsverbund Bremen/Niedersachsen                |
| VGI             | Ingolstädter Verkehrsgesellschaft                   |
| VKG             | Kärntner Linien/Verkehrsverbund Kärnten             |
| VMT             | Verkehrsverbund Mittelthüringen                     |
| VOR             | Verkehrsverbund Ost-Region                          |
| VOS             | Verkehrsgemeinschaft Osnabrück                      |
| VRN             | Verkehrsverbund Rhein-Neckar                        |
| VSN             | Verkehrsverbund Süd-Niedersachsen                   |
| VVT             | Verkehrsverbund Tirol                               |
| VVV             | Verkehrsverbund Vorarlberg                          |
| ZVV             | Zürich public transport                             |

The following profiles are currently being worked on:

| Short Name      | Long Name                                           | Reason                                                                                                  |
|-----------------|-----------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| BART            | Bay Area Rapid Transit                              | Custom certificate needed                                                                               |
| BVG             | Berlin public transport                             | Too many customizations for now                                                                         |
| DB-Busradar-NRW | DB Busradar NRW                                     | Always shows "No Connection"                                                                            |
| IVB             | Innsbrucker Verkehrsbetriebe                        | Custom certificate needed                                                                               |
| KVB             | Kölner Verkehrs-Betriebe                            | Custom certificate needed                                                                               |
| ZVV             | Zürich public transport                             | Authorization Failure                                                                                   |

The following will probably never work:

| Short Name      | Long Name                                           | Reason                                                                                                  |
|-----------------|-----------------------------------------------------|---------------------------------------------------------------------------------------------------------|
| HVV             | Hamburger Verkehrsverbund                           | Shut off endpoint <https://github.com/public-transport/hafas-client/issues/271#issuecomment-1262363078> |
| SBB             | Schweizerische Bundesbahnen                         | Shut off endpoint <https://github.com/public-transport/hafas-client/issues/271#issuecomment-1262363078> |
| SNCB            | Belgian National Railways                           | Shut off endpoint <https://github.com/public-transport/hafas-client/issues/284>                         |
| SNCF            | Société nationale des chemins de fer français       | Shut off endpoint                                                                                       |

Note that some are gated behind non-default feature flags but broken.

## Feature Flags

| Name               | Description                                                 | Note                                                                                      |
|--------------------|-------------------------------------------------------------|-------------------------------------------------------------------------------------------|
| *-profile          | Enable a certain profile                                    | Not every profile gated behind its feature gate may work. Consult above table for details |
| all-profiles       | Enable all default profiles which should work               |                                                                                           |
| rt-multi-thread    | Allow moving the client and hyper-requester between threads |                                                                                           |
| hyper-requester    | Enable requesting data with hyper                           |                                                                                           |
| polylines          | Allow returning polylines from journey results              | Untested                                                                                  |
| rest-server        | Compile an (executable) rest-server                         | Should not be use with libraries                                                          |
| js-error           | Enable JavaScript errors for `hafas_rs::Error`              | Untested                                                                                  |
| js-fetch-requester | Enable requesting data within wasm                          | Untested                                                                                  |
| wasm-bindings      | Enable integration with wasm                                | Untested                                                                                  |

## License

hafas-rs itself is dual-licensed AGPL-3.0-or-later and EUPL-1.2.
