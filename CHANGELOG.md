# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.1] - 2024-03-03

### Fixed

- Rejseplanen profile leading to errors with buses.

## [0.2.0] - 2024-01-09

### Addeed

- Frequency Information to Legs

### Changed

- Coarser `PartialEq` implementation for `Place`.

### Chores

- Clippy fixes.
- Dependency Updates 

## [0.1.0] - 2023-09-24

- Initial Release

[Unreleased]: https://gitlab.com/schmiddi-on-mobile/hafas-rs/-/compare/master...0.2.0
[0.2.0]: https://gitlab.com/schmiddi-on-mobile/hafas-rs/-/compare/0.2.0...0.1.0
[0.1.0]: https://gitlab.com/schmiddi-on-mobile/hafas-rs/-/tags/0.1.0
