#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const RE: Product = Product {
        id: Cow::Borrowed("regional-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("regional train"),
        short: Cow::Borrowed("RE/RB"),
    };
    pub const S: Product = Product {
        id: Cow::Borrowed("urban-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[16]),
        name: Cow::Borrowed("urban train"),
        short: Cow::Borrowed("S"),
    };
    pub const U: Product = Product {
        id: Cow::Borrowed("subway"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("subway"),
        short: Cow::Borrowed("U"),
    };
    pub const TRAM: Product = Product {
        id: Cow::Borrowed("tram"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("tram"),
        short: Cow::Borrowed("Tram"),
    };
    pub const BUS: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Bus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const TAXI: Product = Product {
        id: Cow::Borrowed("dial-a-ride"),
        mode: Mode::Taxi,
        bitmasks: Cow::Borrowed(&[512]),
        name: Cow::Borrowed("dial-a-ride"),
        short: Cow::Borrowed("taxi"),
    };
    pub const ICE: Product = Product {
        id: Cow::Borrowed("long-distance-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[1, 2, 4]),
        name: Cow::Borrowed("long-distance train"),
        short: Cow::Borrowed("ICE/IC/EC/EN"),
    };

    pub const PRODUCTS: &[&Product] = &[&RE, &S, &U, &TRAM, &BUS, &TAXI, &ICE];
}

#[derive(Debug)]
pub struct VrnProfile;

impl Profile for VrnProfile {
    fn url(&self) -> &'static str {
        "https://vrn.hafas.de/bin/mgate.exe"
    }
    fn language(&self) -> &'static str {
        "de"
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Berlin
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        None
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] = json!({"type":"IPH","id":"DB-REGIO-VRN","v":"6000400","name":"VRN"});
        req_json["ver"] = json!("1.34");
        req_json["ext"] = json!("DB.R19.04.a");
        req_json["auth"] = json!({"type":"AID","aid":"p091VRNZz79KtUz5"});
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "EUR"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl VrnProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use std::error::Error;

    use crate::profile::test::{check_journey, check_search};

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(VrnProfile {}, "Frei", "Freinsheim").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(VrnProfile {}, "8000236", "8003932").await
    }
}
