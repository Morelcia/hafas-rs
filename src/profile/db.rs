#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

// TODO: parse Ausstattung, Öffnungszeiten, LocWithDetais, LoadFactors, LineWithAdditionalName,
// JourneyWithPrice, Hints, Codes, IBNR
// TODO: transform JourneysQuery

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const ICE: Product = Product {
        id: Cow::Borrowed("nationalExpress"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[1]),
        name: Cow::Borrowed("InterCityExpress"),
        short: Cow::Borrowed("ICE"),
    };
    pub const IC: Product = Product {
        id: Cow::Borrowed("national"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[2]),
        name: Cow::Borrowed("InterCity & EuroCity"),
        short: Cow::Borrowed("IC/EC"),
    };
    pub const RE: Product = Product {
        id: Cow::Borrowed("regionalExp"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("RegionalExpress & InterRegio"),
        short: Cow::Borrowed("RE/IR"),
    };
    pub const RB: Product = Product {
        id: Cow::Borrowed("regional"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("Regio"),
        short: Cow::Borrowed("RB"),
    };
    pub const S: Product = Product {
        id: Cow::Borrowed("suburban"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[16]),
        name: Cow::Borrowed("S-Bahn"),
        short: Cow::Borrowed("S"),
    };
    pub const B: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Bus"),
        short: Cow::Borrowed("B"),
    };
    pub const F: Product = Product {
        id: Cow::Borrowed("ferry"),
        mode: Mode::Watercraft,
        bitmasks: Cow::Borrowed(&[64]),
        name: Cow::Borrowed("Ferry"),
        short: Cow::Borrowed("F"),
    };
    pub const U: Product = Product {
        id: Cow::Borrowed("subway"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("U-Bahn"),
        short: Cow::Borrowed("U"),
    };
    pub const T: Product = Product {
        id: Cow::Borrowed("tram"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("Tram"),
        short: Cow::Borrowed("T"),
    };
    pub const TAXI: Product = Product {
        id: Cow::Borrowed("taxi"),
        mode: Mode::Taxi,
        bitmasks: Cow::Borrowed(&[512]),
        name: Cow::Borrowed("Group Taxi"),
        short: Cow::Borrowed("Taxi"),
    };

    pub const PRODUCTS: &[&Product] = &[&ICE, &IC, &RE, &RB, &S, &B, &F, &U, &T, &TAXI];
}

#[derive(Debug)]
pub struct DbProfile;

impl Profile for DbProfile {
    fn url(&self) -> &'static str {
        "https://reiseauskunft.bahn.de/bin/mgate.exe"
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        Some("bdI8UVj40K5fvxwf")
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::Europe::Berlin
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }
    fn salt(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["svcReqL"][0]["cfg"]["rtMode"] = json!("HYBRID");
        req_json["client"] = json!({
            "id": "DB",
            "v": "19040000",
            "type": "IPH",
            "name": "DB Navigator"
        });
        req_json["ext"] = json!("DB.R20.12.b");
        req_json["ver"] = json!("1.34");
        req_json["auth"] = json!({
            "type": "AID",
            "aid": "n91dB8Z77MLdoR0K"
        });
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "hafas-rs");
    }

    fn price_currency(&self) -> &'static str {
        "EUR"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl DbProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use crate::profile::test::{check_journey, check_search};
    use std::error::Error;

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(DbProfile {}, "Bayr", "Bayreuth Hbf").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(DbProfile {}, "8011167", "8000261").await
    }
}
