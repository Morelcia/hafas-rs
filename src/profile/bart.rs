#[cfg(feature = "wasm-bindings")]
use crate::client::BoxedProfile;
use crate::{Product, Profile};
use serde_json::{json, Value};
use std::collections::HashMap;
#[cfg(feature = "wasm-bindings")]
use wasm_bindgen::prelude::wasm_bindgen;

mod products {
    use crate::{Mode, Product};
    use std::borrow::Cow;

    pub const BART: Product = Product {
        id: Cow::Borrowed("bart"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[128]),
        name: Cow::Borrowed("BART"),
        short: Cow::Borrowed("BART"),
    };
    pub const REGIONAL_TRAINS: Product = Product {
        id: Cow::Borrowed("regional-train"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[8]),
        name: Cow::Borrowed("regional trains (Caltrain, Capitol Corridor, ACE)"),
        short: Cow::Borrowed("regional trains"),
    };
    pub const BUS: Product = Product {
        id: Cow::Borrowed("bus"),
        mode: Mode::Bus,
        bitmasks: Cow::Borrowed(&[32]),
        name: Cow::Borrowed("Bus"),
        short: Cow::Borrowed("Bus"),
    };
    pub const FERRY: Product = Product {
        id: Cow::Borrowed("ferry"),
        mode: Mode::Watercraft,
        bitmasks: Cow::Borrowed(&[64]),
        name: Cow::Borrowed("Ferry"),
        short: Cow::Borrowed("Ferry"),
    };
    pub const TRAM: Product = Product {
        id: Cow::Borrowed("tram"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[256]),
        name: Cow::Borrowed("Tram"),
        short: Cow::Borrowed("Tram"),
    };
    pub const CABLE_CAR: Product = Product {
        id: Cow::Borrowed("cable-car"),
        mode: Mode::Train,
        bitmasks: Cow::Borrowed(&[4]),
        name: Cow::Borrowed("cable car"),
        short: Cow::Borrowed("cable car"),
    };

    pub const PRODUCTS: &[&Product] = &[&BART, &REGIONAL_TRAINS, &BUS, &FERRY, &TRAM, &CABLE_CAR];
}

#[derive(Debug)]
pub struct BartProfile;

impl Profile for BartProfile {
    fn url(&self) -> &'static str {
        "https://planner.bart.gov/bin/mgate.exe"
    }
    fn language(&self) -> &'static str {
        "en"
    }
    fn checksum_salt(&self) -> Option<&'static str> {
        None
    }
    fn timezone(&self) -> chrono_tz::Tz {
        chrono_tz::America::Los_Angeles
    }
    fn refresh_journey_use_out_recon_l(&self) -> bool {
        true
    }

    fn products(&self) -> &'static [&'static Product] {
        products::PRODUCTS
    }

    fn prepare_body(&self, req_json: &mut Value) {
        req_json["client"] = json!({
            "type": "WEB",
            "id": "BART",
            "name": "webapp"
        });
        req_json["ver"] = json!("1.40");
        req_json["auth"] = json!({
            "type": "AID",
            "aid": "kEwHkFUCIL500dym"
        });
    }

    fn prepare_headers(&self, headers: &mut HashMap<&str, &str>) {
        headers.insert("User-Agent", "my-awesome-e5f276d8fe6cprogram");
    }

    fn price_currency(&self) -> &'static str {
        "USD"
    }
}

#[cfg(feature = "wasm-bindings")]
#[wasm_bindgen]
impl BartProfile {
    #[wasm_bindgen(constructor)]
    pub fn wasm_new() -> BoxedProfile {
        Self.into()
    }
}

#[cfg(test)]
mod test {
    use std::error::Error;

    use crate::profile::test::{check_journey, check_search};

    use super::*;

    #[tokio::test]
    async fn test_search() -> Result<(), Box<dyn Error>> {
        check_search(BartProfile {}, "Rye", "Rye, San Francisco").await
    }

    #[tokio::test]
    async fn test_path_available() -> Result<(), Box<dyn Error>> {
        check_journey(BartProfile {}, "100013296", "100013295").await
    }
}
