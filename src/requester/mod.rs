#[cfg(feature = "hyper-requester")]
pub mod hyper;
#[cfg(feature = "js-fetch-requester")]
pub mod js_fetch;

use crate::Result;
use async_trait::async_trait;
use std::collections::HashMap;

#[cfg_attr(feature = "rt-multi-thread", async_trait)]
#[cfg_attr(not(feature = "rt-multi-thread"), async_trait(?Send))]
pub trait Requester: Send + Sync {
    async fn request(
        &self,
        url: String,
        body: String,
        headers: HashMap<&str, &str>,
    ) -> Result<Vec<u8>>;
}
